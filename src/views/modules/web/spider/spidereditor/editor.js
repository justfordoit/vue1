/**
   * 加载各种图形
   */
export function loadShapes(editor, container) {
  // 定义图形
  var shapes = [{
    name: 'start',
    title: '开始',
    image: '../../../../../../static/img/start.png',
    hidden: true,
    defaultAdd: true
  }, {
    name: 'request',
    image: '../../../../../../static/img/request.png',
    title: '开始抓取',
    desc: '抓取静态HTML页面或者API接口，抓取结果存为resp变量中。<br/>支持方法参考命令提示。'
  }, {
    name: 'variable',
    image: '../../../../../../static/img/variable.png',
    title: '定义变量',
    desc: '定义流程变量。<br/>定义变量有先后顺序，先定义变量后续可以使用，拖动可以交换变量顺序。'
  }, {
    name: 'loop',
    image: '../../../../../../static/img/loop.png',
    title: '循环',
    desc: ''
  }, {
    name: 'forkJoin',
    image: '../../../../../../static/img/forkJoin.png',
    title: '执行结束',
    desc: ''
  }, {
    name: 'comment',
    image: '../../../../../../static/img/comment.png',
    title: '注释',
    desc: '仅仅是注释,毫无作用'
  }, {
    name: 'output',
    image: '../../../../../../static/img/output.png',
    title: '输出',
    desc: '输出流程中的变量结果（仅测试下有用）'
  }, {
    name: 'executeSql',
    image: '../../../../../../static/img/executeSql.png',
    title: '执行SQL',
    desc: '执行sql，需配置数据源，sql执行结果存于变量rs中。<br/>语句类型为：select，返回:List&lt;Map&lt;String,Object&gt;&gt;<br/>语句类型为：selectOne，返回:Map&lt;String,Object&gt;<br/>语句类型为：selectInt，返回:Integer<br/>语句类型为：insert、update、delete，返回:int，批量操作返回int数组<br/>sql中变量必须用 # # 包裹，如：#${title}#'
  }, {
    name: 'function',
    image: '../../../../../../static/img/function.png',
    title: '执行函数',
    desc: '单独执行函数方法，结果不保存为变量'
  }, {
    name: 'process',
    image: '../../../../../../static/img/process.png',
    title: '子流程',
    desc: '执行其他spiderFlow流程，父子流程变量共享'
  }]

  var addShape = function(shape) {
    var image = new Image()
    image.src = shape.image
    image.title = shape.title
    image.id = shape.name
    image.onclick = function(ev) {
      if (shape.desc) {
        layer.tips('(' + shape.name + ')' + shape.title + '<hr/>' + shape.desc, '#' + shape.name, {
          tips: [1, '#3595CC'],
          area: ['auto', 'auto'],
          time: 4000
        })
      }
    }
    if (!shape.hidden) {
      container.appendChild(image)
    }

    if (!shape.disabled) {
      editor.addShape(shape.name, shape.title || 'Label', image, shape.defaultAdd)
    }
  }

  for (var i = 0, len = shapes.length; i < len; i++) {
    addShape(shapes[i])
  }

  // $.ajax({
  //   url: 'spider/shapes',
  //   type: 'post',
  //   dataType: 'json',
  //   async: false,
  //   success: function(shapeExts) {
  //     for (var i = 0, len = shapeExts.length; i < len; i++) {
  //       var shape = shapeExts[i]
  //       addShape(shape)
  //       var image = new Image()
  //       image.src = shape.image
  //       image.title = shape.title
  //       editor.addShape(shape.name, shape.title || 'Label', image, false)
  //       resizeSlideBar()
  //     }
  //   }
  // })
}

